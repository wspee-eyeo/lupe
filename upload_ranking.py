import json
import argparse
import os.path
import zipfile

from google.cloud import bigquery

BIGQUERY_PROJECT = 'lupe-316415'
BIGQUERY_DATASET = 'lupe2'

def init_dataset(bigquery_client, force_init=False):
    dataset_ref = bigquery_client.dataset(BIGQUERY_DATASET)

    NotFound = Exception

    if force_init:
        dataset = bigquery_client.get_dataset(dataset_ref)
        bigquery_client.delete_dataset(dataset, delete_contents=True)

    try:
        dataset = bigquery_client.get_dataset(dataset_ref)
    except NotFound:
        dataset = bigquery.Dataset(dataset_ref)
        bigquery_client.create_dataset(dataset)
        print('Created Dataset {}'.format(dataset.dataset_id))
    else:
        print('Found Dataset {}'.format(dataset.dataset_id))
        

    schema = (
        ('extension_search_position', (
            bigquery.SchemaField('keyword', 'STRING', mode='REQUIRED'),
            bigquery.SchemaField('position', 'INTEGER', mode='REQUIRED'),
            bigquery.SchemaField('name', 'STRING', mode='REQUIRED'),
            bigquery.SchemaField('extension_id', 'STRING', mode='REQUIRED'),
            bigquery.SchemaField('date', 'TIMESTAMP', mode='REQUIRED'),
        )),
    )

    for name, fields in schema:
        table_ref = dataset_ref.table(name)
        try:
            table = bigquery_client.get_table(table_ref)
        except NotFound:
            table = bigquery_client.create_table(bigquery.Table(table_ref, fields))
            print('Created Table {}'.format(table.table_id))
        else:
            print('Found Table {}'.format(table.table_id))

def insert_results(bigquery_client, results):
    dataset_ref = bigquery_client.dataset(BIGQUERY_DATASET)
    table_ref = dataset_ref.table('extension_search_position')
    table = bigquery_client.get_table(table_ref)

    rows = []
    for kw, extensions in results['result'].items():
        for pos, extension in enumerate(extensions):
            rows.append((kw, pos, extension['name'], extension['id'], results['date']))

    print('Inserting {} rows'.format(len(rows)))

    errors = bigquery_client.insert_rows(table, rows)
    if errors:
        print(errors)
    return

def main(input_, force_init):
    results = json.load(open(input_, 'r'))

    client = bigquery.Client(project=BIGQUERY_PROJECT)

    init_dataset(client, force_init)
    insert_results(client, results)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('-i', '--input', required=True)
  parser.add_argument('-D', '--force-init', action='store_true')
  args = parser.parse_args()
  main(args.input, args.force_init)
