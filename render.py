import json
import jinja2
import pprint
import argparse

def main(result, output=None):
  results = json.load(open(result))

  # Prepare results for detailed overview and remember interesting (>0) comparisons
  details, seen = [], set()
  for result in results:
    for tgt in result['targets']:
      if tgt['similarity'] and float(tgt['similarity']) > 0:
        seen.add(tgt['name'])
        details.append((result['source'], tgt))
  details.sort(key=lambda e: e[0]['name'])

  # Create comparison matrix of interesting (>0) comparisons
  head, rows = [], []
  for result in results:
    if result['source']['name'] not in seen:
      continue

    if result['source']['name'] not in head:
      head.append(result['source']['name'])

    cells = [(result['source']['id'], result['source']['name'], '1.0')]
    for tgt in result['targets']:
      if tgt['name'] in seen:
        cells.append((tgt['id'], tgt['name'], tgt['similarity']))
    cells.sort(key=lambda e: e[1])

    rows.append((result['source']['id'], result['source']['name'], cells))

  head.sort()
  rows.sort(key=lambda row: row[1])
  matrix = {'head': head, 'rows': rows}

  env = jinja2.Environment(loader=jinja2.FileSystemLoader('./'))
  template = env.get_template('render.html')

  rendered_output = template.render(results=results, matrix=matrix, details=details)

  if not output:
    print(rendered_output)
  else:
    open(output, 'w').write(rendered_output)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('result')
  parser.add_argument('-o', '--output')
  args = parser.parse_args()
  main(args.result, output=args.output)